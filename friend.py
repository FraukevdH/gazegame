import random
# Define the enemy object by extending pygame.sprite.Sprite

# The surface you draw on the screen is now an attribute of 'enemy'
import pygame
import os
from random import randrange

from constants import *

class Friend(pygame.sprite.Sprite):

    def __init__(self):

        super(Friend, self).__init__()
        self.surf = pygame.Surface( (100, 100))
        sprite_image = pygame.image.load(os.path.join(os.path.join('Assets', 'friend'), str(randrange(1, 5)) + ".png"))
        self.image = pygame.transform.scale(sprite_image,  (100, 100))
        self.move_dir =random.choice(["left", "right"])
        if self.move_dir == "left":
            self.rect = self.image.get_rect(

                center=(
                      pygame.display.get_surface().get_width(),
                    random.randint(0, pygame.display.get_surface().get_height()),

                )
            )
        if self.move_dir == "right":
            self.image = pygame.transform.flip(self.image, True, False)
            self.rect = self.image.get_rect(

                center=(
                    0,
                    random.randint(0, pygame.display.get_surface().get_height()),

                )
            )
        self.next_move = pygame.time.get_ticks() +100
        self.speed = random.randint(1,4)

    # Move the sprite based on speed

    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        if pygame.time.get_ticks() >= self.next_move:
            self.next_move = pygame.time.get_ticks() +100
            if self.move_dir == "left":
                self.rect.move_ip(-self.speed, 0)
            if self.move_dir == "right":
                self.rect.move_ip(self.speed, 0)

        if self.rect.right < 0:
            self.kill()

        if self.rect.left > pygame.display.get_surface().get_width():
            self.kill()
      #  move_vec = pygame.math.Vector2(self.rect.center) - pygame.math.Vector2(DISPSIZE[0] / 2, DISPSIZE[1] / 2)
      #  self.rect.move_ip(self.speed * move_vec[0], self.speed * move_vec[1])

