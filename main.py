import os
import sys
from random import randrange

import pygame
import pygaze
from pygaze._screen import pygamescreen

import highscores
from constants import *
from pyglet import clock

from pygaze import libtime
from pygaze.libscreen import Display, Screen
from pygaze.libinput import Keyboard
from pygaze.display import Display

from pygaze.eyetracker import EyeTracker
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_SPACE,
    KEYDOWN,
    QUIT
)
from enemy import Enemy
from friend import Friend

pygame.init()
pygame.display.set_caption("Gaze Shooter")
disp = Display()
tracker = EyeTracker(disp)
tracker_screen = Screen()
game_surf = pygamescreen

# create keyboard object
keyboard = Keyboard()

# Create a custom event for adding a new enemy
ADDENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENEMY, 100)

ADDFRIEND = pygame.USEREVENT + 2
pygame.time.set_timer(ADDFRIEND, 200)

pygame.event.set_allowed((QUIT, ADDFRIEND, ADDENEMY))

# Create groups to hold enemy sprites and all sprites
# - enemies and  friends is used for collision detection and position updates
# - all_sprites is used for rendering
enemies = pygame.sprite.Group()
friends = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()

# create eyelink objecy
# display object
disp = Display()
eyetracker = EyeTracker(disp)

# eyelink calibration
eyetracker.calibrate()

# pygame
pygame.display.set_mode((DISPSIZE[0], DISPSIZE[1]))


def main():
    #   disp.show()
    running = True
    points = 0
    keyboard.set_keylist(KEYLIST)
    keyboard.set_timeout(KEYTIMEOUT)
    spawn_time_friend = SPAWNTIME_FRIEND
    spawn_time_enemy = SPAWNTIME_ENEMY

    events = pygame.event.get()

    while running:
        gazepos = eyetracker.sample()
        # get keypress
        key, presstime = keyboard.get_key('default', 'default')

        spawn_time_enemy = spawn_enemies(spawn_time_enemy)
        spawn_time_friend = spawn_friends(spawn_time_friend)
       # print(key)
        # handle input
        if key:
            #print(key)
            if key == 'escape':
                running = False
                eyetracker.stop_recording()
                pygame.quit()

            if key == 'space' or key == 'w':
                for enemy in enemies:
                    if ((gazepos[0] - enemy.rect.centerx) ** 2 + (
                            gazepos[1] - enemy.rect.centery) ** 2) ** 0.5 < STIMSIZE / 2:
                        #  game_screen.copy(hitscreen)
                        enemy.kill()
                        points += PPH
                        print("enemy killed")

                    else:
                        # game_screen.copy(misscreen)
                        points += PPM
                        print("missed")
                for friend in friends:
                    if ((gazepos[0] - friend.rect.centerx) ** 2 + (
                            gazepos[1] - friend.rect.centery) ** 2) ** 0.5 < STIMSIZE / 2:
                        # game_screen.copy(hitscreen)
                        friend.kill()
                        points += PPH
                        print("friend killed")

                    else:
                        #  game_screen.copy(misscreen)
                        points += PPM

        # for event in pygame.event.get():
        #     print(event.type)
        #     if pygame.event.get(QUIT):
        #          running = False
        #          eyetracker.stop_recording()
        #          pygame.quit()
        #
        #     if pygame.event.get(ADDENEMY):
        #         spawn_enemies()
        #     if pygame.event.get(ADDFRIEND):
        #         spawn_friends()

        draw_window(gazepos)

        for entity in all_sprites:
            entity.update()


def draw_window(gazepos):
    tracker_screen.clear()
    game_screen_comb = pygame.Surface(DISPSIZE)
    game_screen_comb.fill(BGCOLOR2)
    # Draw all sprites
    for entity in all_sprites:
        game_screen_comb.blit(entity.image, entity.rect)

    # Flip everything to the display --same as update()

#    pygame.display.flip()
    # draw crosshair

    #    tracker_screen.draw_image( pygame.image.load(os.path.join(os.path.join('Assets', 'enemy'),  str(randrange(6, 10))+".png")))
    tracker_screen.draw_image(game_screen_comb)
    tracker_screen.draw_circle(colour=FGC, pos=gazepos, r=13, pw=2, fill=False)
    tracker_screen.draw_line(colour=FGC, spos=(gazepos[0] - 15, gazepos[1]), epos=(gazepos[0] + 15, gazepos[1]), pw=2)
    tracker_screen.draw_line(colour=FGC, spos=(gazepos[0], gazepos[1] - 15), epos=(gazepos[0], gazepos[1] + 15), pw=2)


    #score display
    tracker_screen.draw_text("Points: ")
    disp.fill(screen=tracker_screen)
    disp.show()
    clock.tick(FPS)


def spawn_enemies(spawn_time_enemy):
    if pygame.time.get_ticks() > spawn_time_enemy:
        new_enemy = Enemy()
        enemies.add(new_enemy)
        all_sprites.add(new_enemy)
        spawn_time_enemy = pygame.time.get_ticks()+ spawn_time_enemy

    return spawn_time_enemy


def spawn_friends(spawn_time_friend):
    if pygame.time.get_ticks() > spawn_time_friend:
    # Create the new enemy and add it to sprite groups
        new_friend = Friend()
        friends.add(new_friend)
        all_sprites.add(new_friend)
        spawn_time_friend = pygame.time.get_ticks()+ spawn_time_friend
    return spawn_time_friend

if __name__ == "__main__":
    main()
